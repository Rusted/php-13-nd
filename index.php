<?php
require_once 'functions.php';
$pdo = getConnection();
$employees = getAllEmployees($pdo);
$positions = getAllPositions($pdo);
$employeeCounts = getEmployeeCounts($pdo);
?>
<html>
<body>
    <h1>Darbuotojai</h1>
    <table border="3">
        <tr>
            <th>Vardas</th>
            <th>Pavardė</th>
            <th>Išsilavinimas</th>
            <th>Atlyginimas</th>
            <th>Telefonas</th>
            <?php foreach ($employees as $employee) {?>

                <tr>
                <td><a href="employee.php?id=<?php echo $employee['id']; ?>"><?php echo $employee['name']; ?></a></td>
                <td><a href="employee.php?id=<?php echo $employee['id']; ?>"><?php echo $employee['surname']; ?></a></td>
                <td><?php echo $employee['education']; ?></td>
                <td><?php echo $employee['salary']; ?></td>
                <td><?php echo $employee['phone']; ?></td>
                </tr>
            <?php }?>
        </tr>
    </table>
    <h1>Pareigos</h1>
    <table border="3">
        <tr>
            <th>Pavadinimas</th>
            <th>Bazinis atlyginimas</th>
            <th>Darbuotojų skaičius</th>
            <?php foreach ($positions as $position) {?>
                <tr>
                    <td><a href="position.php?id=<?php echo $position['id']; ?>"><?php echo $position['name']; ?></a></td>
                    <td><?php echo $position['base_salary']; ?></td>
                    <td><?php echo $employeeCounts[$position['id']]; ?></td>

                </tr>
                </a>
            <?php }?>
        </tr>
    </table>
</body>

</html>
